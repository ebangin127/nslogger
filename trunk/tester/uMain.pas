unit uMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ComCtrls, Vcl.StdCtrls, Vcl.ExtCtrls, Math, DateUtils,
  Vcl.Imaging.pngimage, System.UITypes,
  uRandomBuffer, uGSTestThread, uGSList, uSSDInfo, uTrimCommand,
  uDiskFunctions, uSetting, uRetSel;

const
  WM_AFTER_SHOW = WM_USER + 300;

  OuterPadding = 10;
  InnerPadding = 5;
  HalfPadding = InnerPadding;

  CapacityOf128GB = 250069680;

type
  TfMain = class(TForm)
    gStatus: TGroupBox;
    lAvgLatency: TLabel;
    lMaxLatency: TLabel;
    pAvgLatency: TProgressBar;
    pMaxLatency: TProgressBar;
    sAvgLatency: TStaticText;
    sMaxLatency: TStaticText;
    gFirstSet: TGroupBox;
    gAlert: TGroupBox;
    lAlert: TListBox;
    pFFR: TProgressBar;
    sFFR: TStaticText;
    lFFR: TLabel;
    pTestProgress: TProgressBar;
    sTestProgress: TStaticText;
    lTestProgress: TLabel;
    lMaxAlertL: TLabel;
    lAvgAlertL: TLabel;
    lMaxAlertR: TLabel;
    lAvgAlertR: TLabel;
    lFFRR: TLabel;
    lFFRL: TLabel;
    iForceReten: TImage;
    lForceReten: TLabel;
    iSave: TImage;
    lSave: TLabel;
    iLogo: TImage;
    lDest: TLabel;
    sDestPath: TStaticText;
    sDestModel: TStaticText;
    sDestSerial: TStaticText;
    lDestTBW: TLabel;
    sDestTBW: TStaticText;
    sRetention: TStaticText;
    lRetention: TLabel;
    lMaxFFR: TLabel;
    sMaxFFR: TStaticText;
    tSave: TTimer;
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure bSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure bForceRetenClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure lForceRetenMouseEnter(Sender: TObject);
    procedure lForceRetenMouseLeave(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FDiskNum: Integer;
    FDestDriveModel: String;
    FDestDriveSerial: String;
    FDestDriveCapacity: INT64;
    FDestTBW: Integer;
    FRetentionTBW: Integer;
    FMaxFFR: Integer;
    FSaveFilePath: String;
    FNeedRetention: Boolean;
    FRepeatRetention: Boolean;

    procedure WmAfterShow(var Msg: TMessage); message WM_AFTER_SHOW;
    procedure ResizeStatusComponents(BasicTop: Integer;
                                 InnerPadding, OuterPadding: Integer;
                                 gParent: TGroupBox;
                                 lName: TLabel; sDynText: TStaticText;
                                 pProgress: TProgressBar;
                                 lProgressL, lProgressR: TLabel); inline;

    procedure gStatusResize;
    procedure gFirstSetResize;
    procedure gChangeStateResize;
    procedure gAlertResize;
  public
    property NeedRetention: Boolean read FNeedRetention;
    property DriveModel: String read FDestDriveModel;
    property DriveSerial: String read FDestDriveSerial;
    property RepeatRetention: Boolean read FRepeatRetention;

    function GetLogLine(Name: String; Contents: String = ''): String;
    { Public declarations }
  end;

var
  fMain: TfMain;
  TestThread: TGSTestThread;
  AppPath: String;

implementation

{$R *.dfm}

procedure TfMain.bForceRetenClick(Sender: TObject);
begin
  FNeedRetention := true;
  lAlert.Items.Add(GetLogLine('임의 리텐션 테스트'));

  if TestThread <> nil then
  begin
    TestThread.Terminate;
    WaitForSingleObject(TestThread.Handle, 60);

    FreeAndNil(TestThread);

    if NeedRetention then
    begin
      fRetSel := TfRetSel.Create(self,
                                 '\\.\PhysicalDrive' + IntToStr(FDiskNum));
      fRetSel.ShowModal;
    end;
  end;

  Close;
end;

procedure TfMain.bSaveClick(Sender: TObject);
begin
  FNeedRetention := false;
  Close;
end;

procedure TfMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if (TestThread <> nil) and
      (FRepeatRetention = false) then
    TestThread.AddToAlert(
      GetLogLine('테스트 정상 종료', '쓰기량 - '
                                     + GetByte2TBWStr(TestThread.HostWrite)
                                     + ' / 평균 지연 - '
                                     + Format('%.2f%s', [TestThread.AvgLatency,
                                                         'ms'])
                                     + ' / 최대 지연 - '
                                     + Format('%.2f%s', [TestThread.MaxLatency,
                                                         'ms'])));
end;

procedure TfMain.FormCreate(Sender: TObject);
var
  MessageResult: Integer;
begin
  MessageResult :=
    Application.MessageBox(
      PChar('라이선스'
            + Chr(13) + Chr(10) +
              Chr(13) + Chr(10) +
            '1. 본 프로그램은 2014/09/07 기준 SSDSAMO 닉네임'
            + Chr(13) + Chr(10) +
            '   Winfix, 머쨍, 야간순찰 - 3명만 사용할 수 있습니다'
            + Chr(13) + Chr(10) +
              Chr(13) + Chr(10) +
            '2. 본 프로그램은 JEDEC의 기술 문서인 JESD219A의 수명 측정 표준인'
            + Chr(13) + Chr(10) +
            '   Client Workload 수행 및 결과 획득을 위해서만 사용할 수 있습니다'
            + Chr(13) + Chr(10) +
              Chr(13) + Chr(10) +
            '3. 본 프로그램의 허가받지 않은 수정 혹은 배포를 금지합니다.'
            + Chr(13) + Chr(10) +
              Chr(13) + Chr(10) +
            '4. 본 프로그램의 허가받지 않은 이용, 대한민국 법에 어긋나는'
            + Chr(13) + Chr(10) +
            '   목적으로의 이용, 변형 행위를 할 시 그로 인해 발생하는'
            + Chr(13) + Chr(10) +
            '   민/형사상 책임은 사용자가 집니다.'
            + Chr(13) + Chr(10) +
              Chr(13) + Chr(10) +
            '이에 동의하시면 확인을 눌러주세요.'),
      PChar(Caption + ' 라이선스'),
      MB_OKCANCEL  + MB_ICONEXCLAMATION);

  if MessageResult <> 1 then
  begin
    Application.MessageBox(PChar('동의하지 않으시는 경우 프로그램을 사용하실' +
                                 ' 수 없습니다'),
                           PChar(Caption + ' 라이선스'), MB_OK + MB_ICONERROR);
    Application.Terminate;
  end;


  AppPath := ExtractFilePath(Application.ExeName);

  sDestPath.Caption := '';
  sDestModel.Caption := '';
  sDestSerial.Caption := '';
end;

procedure TfMain.FormDestroy(Sender: TObject);
var
  NeedRetention: Boolean;
begin
  if TestThread <> nil then
  begin
    TestThread.Terminate;
    WaitForSingleObject(TestThread.Handle, 60);

    NeedRetention := TestThread.ExitCode = EXIT_RETENTION;

    FreeAndNil(TestThread);

    if NeedRetention then
    begin
      fRetSel := TfRetSel.Create(self,
                                 '\\.\PhysicalDrive' + IntToStr(FDiskNum));
      fRetSel.ShowModal;
    end;
  end;
end;

procedure TfMain.FormResize(Sender: TObject);
const
  BUTTON_MINIMUM_HEIGHT = 65;
begin
  //가로
  //한 칸 10 + 반 칸 5
  gFirstSet.Left := (ClientWidth div 2) + HalfPadding;
  gStatus.Width := (ClientWidth div 2) - (OuterPadding + HalfPadding);
  gFirstSet.Width := (ClientWidth div 2) - (OuterPadding + HalfPadding);
  gAlert.Width := ClientWidth - (OuterPadding shl 1);

  //세로 - 순서가 바뀌어선 안 됨
  gStatus.Top := iLogo.Top + iLogo.Height + OuterPadding;
  gFirstSet.Top := gStatus.Top;
  gFirstSet.Height := ((ClientHeight - (OuterPadding shl 1)) div 2)
                       - HalfPadding;
  gStatus.Height := gFirstSet.Height;
  gAlert.Top := gStatus.Top + gStatus.Height + OuterPadding;
  gAlert.Height := ClientHeight - (gStatus.Top + gStatus.Height
                                    + (OuterPadding shl 1));

  //각 컴포넌트에 크기조절 요청
  gStatusResize;
  gFirstSetResize;
  gChangeStateResize;
  gAlertResize;
end;

procedure TfMain.FormShow(Sender: TObject);
begin
  PostMessage(Self.Handle, WM_AFTER_SHOW, 0, 0);
end;

procedure TfMain.gStatusResize;
var
  UnitSize: Integer;
  CurrBasicTop: Integer;
begin
  UnitSize := sTestProgress.Height + lTestProgress.Height
              + (OuterPadding shl 1);

  //테스트 진행
  CurrBasicTop := OuterPadding;
  lTestProgress.Top := CurrBasicTop;
  sTestProgress.Top := CurrBasicTop;
  pTestProgress.Top := sTestProgress.Top + sTestProgress.Height + InnerPadding;
  pTestProgress.Width := gStatus.Width - pTestProgress.Left;
  ResizeStatusComponents(CurrBasicTop, InnerPadding, OuterPadding, gStatus,
                         lTestProgress, sTestProgress, pTestProgress, nil,
                         nil);

  //평균 지연
  Inc(CurrBasicTop, UnitSize);
  ResizeStatusComponents(CurrBasicTop, InnerPadding, OuterPadding, gStatus,
                         lAvgLatency, sAvgLatency, pAvgLatency, lAvgAlertL,
                         lAvgAlertR);

  //최대 지연
  Inc(CurrBasicTop, UnitSize);
  ResizeStatusComponents(CurrBasicTop, InnerPadding, OuterPadding, gStatus,
                         lMaxLatency, sMaxLatency, pMaxLatency, lMaxAlertL,
                         lMaxAlertR);

  //기능 실패율
  Inc(CurrBasicTop, UnitSize);
  ResizeStatusComponents(CurrBasicTop, InnerPadding, OuterPadding, gStatus,
                         lFFR, sFFR, pFFR, lFFRL, lFFRR);
end;

procedure TfMain.lForceRetenMouseEnter(Sender: TObject);
begin
  if Sender is TLabel then
    TLabel(Sender).Font.Color := clHighlight;
end;

procedure TfMain.lForceRetenMouseLeave(Sender: TObject);
begin
  if Sender is TLabel then
    TLabel(Sender).Font.Color := clWindowText;
end;

procedure TFMain.ResizeStatusComponents(BasicTop: Integer;
                                        InnerPadding, OuterPadding: Integer;
                                        gParent: TGroupBox;
                                        lName: TLabel; sDynText: TStaticText;
                                        pProgress: TProgressBar;
                                        lProgressL, lProgressR: TLabel);
begin
  lName.Top := BasicTop;
  sDynText.Top := BasicTop;
  pProgress.Top := sDynText.Top + sDynText.Height + InnerPadding;
  pProgress.Width := gParent.Width - (pProgress.Left shl 1);

  if lProgressR <> nil then
  begin
    lProgressL.Top := pProgress.Top;
    lProgressR.Top := pProgress.Top;

    lProgressR.Left := pProgress.Left + pProgress.Width +
                        + (pProgress.Left - lProgressL.Left - lProgressL.Width);
  end;
end;

procedure TfMain.gFirstSetResize;
var
  UnitSize: Integer;
  CurrBasicTop: Integer;
begin
  UnitSize := lDest.Height + OuterPadding + InnerPadding;

  //대상 주소
  CurrBasicTop := OuterPadding;
  lDest.Top := CurrBasicTop;
  lDest.Left := lTestProgress.Left;
  sDestPath.Top := lDest.Top;
  sDestPath.Left := lDest.Left + lDest.Width + OuterPadding;

  //대상 모델
  Inc(CurrBasicTop, UnitSize - OuterPadding);
  sDestModel.Top := CurrBasicTop;
  sDestModel.Left := sDestPath.Left;

  //대상 시리얼
  Inc(CurrBasicTop, UnitSize - OuterPadding);
  sDestSerial.Top := CurrBasicTop;
  sDestSerial.Left := sDestPath.Left;

  //목표 TBW
  Inc(CurrBasicTop, UnitSize);
  lDestTBW.Left := lDest.Left;
  lDestTBW.Top := CurrBasicTop;
  sDestTBW.Top := CurrBasicTop;
  sDestTBW.Left := lDestTBW.Left + lDestTBW.Width + OuterPadding;

  //리텐션 테스트 주기
  Inc(CurrBasicTop, UnitSize);
  lRetention.Left := lDest.Left;
  lRetention.Top := CurrBasicTop;
  sRetention.Top := CurrBasicTop;
  sRetention.Left := lRetention.Left + lRetention.Width + OuterPadding;

  //기능 실패율 상한선
  Inc(CurrBasicTop, UnitSize);
  lMaxFFR.Left := lDest.Left;
  lMaxFFR.Top := CurrBasicTop;
  sMaxFFR.Top := CurrBasicTop;
  sMaxFFR.Left := lMaxFFR.Left + lMaxFFR.Width + OuterPadding;
end;

procedure TfMain.gChangeStateResize;
var
  UnitSize: Integer;
  ForceRetenWidth: Integer;
  LastFontSize: Integer;
  MiddlePoint: Integer;
begin
  UnitSize := (gStatus.Width - (OuterPadding shl 1)) shr 1;

  iForceReten.Width := gStatus.Width div 10;
  iForceReten.Height := iForceReten.Width;
  iSave.Width := gStatus.Width div 10;
  iSave.Height := iSave.Width;

  lForceReten.Left := iForceReten.Left + iForceReten.Width + InnerPadding;
  LastFontSize := lForceReten.Font.Size;
  ForceRetenWidth := lForceReten.Left + lForceReten.Width;
  lForceReten.Font.Size := 9;
  while ForceRetenWidth < UnitSize do
  begin
    LastFontSize := lForceReten.Font.Size;
    lForceReten.Font.Size := lForceReten.Font.Size + 1;
    ForceRetenWidth := lForceReten.Left + lForceReten.Width;
  end;
  lForceReten.Font.Size := LastFontSize;

  MiddlePoint := Min(gStatus.Height - lForceReten.Height - OuterPadding
                                     + (lForceReten.Height shr 1),
                     gStatus.Height - iForceReten.Height - OuterPadding)
                                     + (iForceReten.Height shr 1);
  lForceReten.Top := MiddlePoint - (lForceReten.Height shr 1);
  iForceReten.Top := MiddlePoint - (iForceReten.Height shr 1);

  iSave.Left := (gStatus.Width shr 1) + iForceReten.Left;
  lSave.Font.Size := LastFontSize;
  lSave.Left := iSave.Left + iSave.Width + InnerPadding;
  lSave.Top := lForceReten.Top;
  iSave.Top := iForceReten.Top;
end;

function TfMain.GetLogLine(Name, Contents: String): String;
begin
  result := FormatDateTime('[yyyy/mm/dd hh:nn:ss] ', Now) + Name;
  if Contents <> '' then
    result := result + ': ' + Contents;
end;

procedure TfMain.gAlertResize;
begin
  lAlert.Width := gAlert.Width - (lAlert.Left shl 1);
  lAlert.Height := gAlert.Height - (OuterPadding shl 1);
end;

procedure TfMain.WmAfterShow(var Msg: TMessage);
var
  SSDInfo: TSSDInfo;
begin
  fSetting := TfSetting.Create(self);
  fSetting.ShowModal;

  FDiskNum := fSetting.GetDriveNum;
  if FDiskNum = -1 then
  begin
    Close;
    exit;
  end;

  FSaveFilePath := fSetting.SavePath;

  SSDInfo := TSSDInfo.Create;
  SSDInfo.SetDeviceName(FDiskNum);
  FDestTBW := StrToInt(fSetting.eDestTBW.Text);
  FRetentionTBW := StrToInt(fSetting.eRetentionTBW.Text);
  FDestDriveModel := SSDInfo.Model;
  FDestDriveSerial := SSDInfo.Serial;
  FDestDriveCapacity := floor(SSDInfo.UserSize
                              / 2 / 1024 / 1000 / 1000 * 1024 * 1.024); //In GB
  FMaxFFR := StrToInt(fSetting.eFFR.Text);

  sDestPath.Caption := '\\.\PhysicalDrive' + IntToStr(FDiskNum);
  sDestModel.Caption := FDestDriveModel
                        + ' (' + IntToStr(FDestDriveCapacity) + 'GB)';
  sDestSerial.Caption := FDestDriveSerial;

  sDestTBW.Caption := IntToStr(FDestTBW) + 'TBW / ' +
                      GetDayStr((FDestTBW shl 10) / 10);
  sRetention.Caption := IntToStr(FRetentionTBW) + 'TBW / ' +
                        GetDayStr((FRetentionTBW shl 10) / 10);
  sMaxFFR.Caption := IntToStr(FMaxFFR) + '%';

  Application.ProcessMessages;

  TestThread := TGSTestThread.Create(fSetting.TracePath,
                                     true, SSDInfo.UserSize shr 9);
  FRepeatRetention := false;
  if fSetting.LoadedFromFile then
  begin
    TestThread.Load(fSetting.SavePath + 'settings.ini');

    if TestThread.NeedVerify then
    begin
      fRetSel := TfRetSel.Create(self,
                                 '\\.\PhysicalDrive' + IntToStr(FDiskNum));
      fRetSel.SetMode(rsmVerify, fSetting.SavePath + 'compare_error_log.txt');
      fRetSel.ShowModal;

      if fRetSel.bStart.Visible = false then
        TestThread.AddToAlert(
          GetLogLine('리텐션 테스트 종료',
                     'UBER - ' + FloatToStr(fRetSel.UBER)))
      else
        TestThread.AddToAlert(
          GetLogLine('리텐션 테스트 검증 취소'));

      FreeAndNil(fRetSel);

      if MessageDlg('리텐션 테스트를 반복하시겠습니까?',
                    mtWarning, mbOKCancel, 0) = mrOK then
      begin
        FNeedRetention := true;
        FRepeatRetention := true;
      end;
    end;
  end
  else
  begin
    fRetSel := TfRetSel.Create(self,
                               '\\.\PhysicalDrive' + IntToStr(FDiskNum));
    fRetSel.SetMode(rsmPreCond, fSetting.SavePath);
    fRetSel.ShowModal;
    TestThread.SetHostWrite(fRetSel.Written);
    TestThread.AddToAlert(
      GetLogLine('테스트 사전 준비 완료', '쓰기량 - '
                                          + GetByte2TBWStr(fRetSel.Written)));
    FreeAndNil(fRetSel);
  end;
  TestThread.SetDisk(FDiskNum);

  TestThread.MaxLBA := SSDInfo.UserSize;
  TestThread.OrigLBA := CapacityOf128GB;
  TestThread.Align := 512;
  TestThread.MaxFFR := FMaxFFR;

  TestThread.MaxHostWrite := FDestTBW;
  TestThread.RetentionTest := FRetentionTBW;

  TestThread.AssignSavePath(FSaveFilePath);
  TestThread.AssignBufferSetting(16 shl 10, 100);
  TestThread.AssignAlertPath(FSaveFilePath + 'alert.txt');

  FreeAndNil(SSDInfo);
  if FRepeatRetention then
  begin
    Close;
    exit;
  end;

  TestThread.StartThread;
end;
end.
